/*
    Name: AccountHandler
    Description: Custom Rest API Resource to get Account details, Insert and Update the Account Details and delete the Account.
    CreatedBy: Arun Jain
    CreatedDate: 10/07/2017
*/

@RestResource(urlMapping='/HandleAccount/*')
global with sharing class AccountHandler{
    //Store the Message Value for Invalid Field
    private Static String invalidFieldMessage = 'Field does not exists : {0}';
    /*
        Name: getAccountDetail
        @param:
        Description: Get the Id of Account and Then return Account
    */
    @HttpGet
    global static Account getAccountDetail(){
        String accountId = RestContext.request.params.get('accountId');
        Account objAccount = [SELECT Id,Name 
                              FROM Account 
                              WHERE Id=:accountId];
        return objAccount;
    }
    
    /*
        This method works fine in case of when we pass only these arguments and with the same name
        Issue : Can't be use because the end user dosen't the method argument names.
        @HttpPost
        global static String insertAccount(String name,String phone, String website, Integer numberOfEmployees, String industry){
                Account account = new Account();
                account.Name = name;
                account.Phone = phone;
                account.Website = website;
                account.NumberOfEmployees = numberOfEmployees;
                account.Industry = industry;
                
                insert account;
                return 'Record Inserted';
        }
    */
    
    
    /*
        Name:insertAccount
        @param:
        Description: Fetch the Json from RestContext request and Insert New Account
    */
    @HttpPost
    global static void insertAccount(){
        RestRequest request = RestContext.request;
        List<String> lstMessages = new List<String>();
        try{
            //Fetch JSON from Request Body and convert into List of Objects
            List<Object> lstRequestBody = (List<Object>)JSON.deserializeUntyped(request.requestBody.toString());
            //List for Correct Objects
            List<Object> lstObjects = new List<Object>();
            for(Object obj : lstRequestBody){

                Map<String,Object> m = (Map<String,Object>)obj;
                //Check Type field exists or not
                if(m.keyset().contains('type') == false){
                    m.put('message',Label.TypeNotExistsMessage);
                    lstMessages.add(JSON.serialize(obj));
                }
                //Check Reference_Id__c field exists or not
                else if(m.keyset().contains('ReferenceId') == false){
                    m.put('message',Label.ReferenceIdNotExists);
                    lstMessages.add(JSON.serialize(obj));
                }
                //Correct Object to insert
                else{
                    lstObjects.add(obj);
                }
            }
            if(lstObjects.size()>0){
                Map<String,SObject> mapInsertedObjects = new Map<String,SObject>();
                //List for Dynamic Object
                List<SObject> lstDynamicObject = new List<SObject>();
                for(Object obj : lstObjects){
                    Boolean checkFields = true;
                    Map<String,Object> m = (Map<String,Object>)obj;

                    SObjectType objType = Schema.getGlobalDescribe().get((String)m.get('type'));
                    //    Fetch the Field Map of That Object.
                    Map<String,Schema.SObjectField> mapfields = objType.getDescribe().fields.getMap();
                    //    Check whether field exists or not.
                    for(Object obj2 : m.keyset()){
                        String strFieldName = (String)obj2;
                        if(strFieldName.equalsIgnoreCase('ReferenceId')== false && strFieldName.equalsIgnoreCase('type')== false){
                            if(mapfields.keyset().contains((strFieldName).toLowerCase()) == false){
                                m.put('message',String.format(invalidFieldMessage , new String []{strFieldName}));
                                lstMessages.add(JSON.serialize(obj));
                                checkFields = false;
                            }
                        }
                    }
                    //    If all fields are valid.
                    if(checkFields == true){
                        SObject dynamicObject = objType.newSObject();
                        for(Object obj3 : m.keyset()){
                            String strFieldName = (String)obj3;
                            if(strFieldName.equalsIgnoreCase('ReferenceId') == false && strFieldName.equalsIgnoreCase('type') == false){
                                Schema.DisplayType fielddataType = mapfields.get(strFieldName).getDescribe().getType();
                                //    Fetching Datatype of Field.
                                if(fielddataType == Schema.DisplayType.Integer){
                                    dynamicObject.put(strFieldName,Integer.valueof(m.get(strFieldName)));
                                }
                                else{
                                    dynamicObject.put(strFieldName,m.get(strFieldName));
                                }
                            }
                        }
                        lstDynamicObject.add(dynamicObject);
                        mapInsertedObjects.put((String)m.get('ReferenceId'),dynamicObject);
                    }
                }

                insert lstDynamicObject;
                
                for(String refId : mapInsertedObjects.keySet()){
                    lstMessages.add('{"message":"'+Label.RecordInsertedMessage+'","Id":"'+mapInsertedObjects.get(refId).Id+'","ReferenceId":"'+refId+'"}');
                }
            }
            
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf('['+String.join(lstMessages,',')+']');
        }
        catch(JSONException e){
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf('{"message":"'+Label.InvalidJsonFormMessage+'"}');
        }      
    }
}