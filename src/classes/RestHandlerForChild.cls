/*
    Name: RestHandlerForChild -- Checking
    Description: Custom REST API Resource to insert Records of object along with the children.
    CreatedBy: Arun Jain
    CreatedDate: 11/07/2017
*/
@RestResource(urlMapping='/HandleWithChild/*')
global with sharing class RestHandlerForChild{
	/*
		Name: insertRecords
		Param: 
		Description: Method will Insert the Records get by RestRequest JSON
	*/
	@HttpPost
	global static void insertRecords(){
		RestRequest restRequest = RestContext.request;
		List<Object> lstJSONObjects;
		List<String> lstMessages = new List<String>();
		List<SObject> lstSfParentObject = new List<SObject>();
		Map<String,SObject> mapRefSFParentObject = new Map<String,SObject>();
		
		try{
			lstJSONObjects = (List<Object>)JSON.deserializeUntyped(restRequest.requestBody.toString());
		}
		catch(Exception e){
            responseMessage(400,'{"statuscode":"400","message":"'+Label.InvalidJsonFormMessage+'"}');
            return;
		}

		for(Object rawParentObject : lstJSONObjects){
			Map<String,Object> mapRawParentObject = (Map<String,Object>) rawParentObject;
			if(mapRawParentObject.containsKey('type') == false){
				mapRawParentObject.put('Message',Label.TypeNotExistsMessage);
				lstMessages.add(JSON.serialize(rawParentObject));
				responseMessage(400,'['+String.join(lstMessages,',')+']');
           	 	return;
			}
			else if(mapRawParentObject.containsKey('referenceId') == false){
				mapRawParentObject.put('Message',Label.ReferenceIdNotExists);
				lstMessages.add(JSON.serialize(rawParentObject));
				responseMessage(400,'['+String.join(lstMessages,',')+']');
           	 	return;
			}
			else{
				SObjectType sfTypeParent = Schema.getGlobalDescribe().get((String)mapRawParentObject.get('type'));
				String validate = validateObjectField(sfTypeParent,mapRawParentObject);
				if(String.isEmpty(validate) == true){
					SObject sobj = createObject(sfTypeParent,mapRawParentObject);
					mapRefSFParentObject.put((String)mapRawParentObject.get('referenceId'),sobj);
                    lstSfParentObject.add(sobj);
				}
				else{
					mapRawParentObject.put('message',String.format(Label.InvalidFieldNameMessage , new String []{validate}));
                    lstMessages.add(JSON.serialize(rawParentObject));
   	 				responseMessage(400,'['+String.join(lstMessages,',')+']');
           	 		return;
				}
			}
		}

		Savepoint sp = Database.setSavepoint();

		try{
			insert lstSfParentObject;
				
		}
		catch(Exception e){
			Database.rollback(sp);
			
			responseMessage(400,'{"message":"Record Insertion Failed","statuscode":"400"}');
            return;
		}

		List<SObject> lstSfChildObjects = new List<SObject>();
		Map<String,SObject> mapRefSFChildObject = new Map<String,SObject>();
		for(Object rawParentObject : lstJSONObjects){
			Map<String,Object> mapRawParent = (Map<String,Object>)rawParentObject;
			if(mapRawParent.containsKey('children') == true){
				SObjectType objChildType;
				String strRelatedField;
				for(Object rawChildren : (List<Object>)mapRawParent.get('children')){
					Map<String,Object> mapRawChildren = (Map<String,Object>)rawChildren;
	        		if(mapRawChildren.containsKey('relationshipName') == false){
	        			mapRawChildren.put('Message','Relationship Name does not Exists');
	        			lstMessages.add(JSON.serialize(rawParentObject));
	        			Database.rollback(sp);
	        				
	                    responseMessage(400,'['+String.join(lstMessages,',')+']');
	   	 				return;
	        		}
	        		else if(mapRawChildren.containsKey('records') == false){
	        			mapRawChildren.put('Message','Records does not Exists.');
	        			lstMessages.add(JSON.serialize(rawParentObject));
	        			Database.rollback(sp);
	        			System.debug('Records Does Not Exists.');
	   	 				responseMessage(400,'['+String.join(lstMessages,',')+']');
	   	 				return;
	        		}
	        		else{
            			String relationshipName = (String)mapRawChildren.get('relationshipName');
            			SObjectType sfTypeParent = Schema.getGlobalDescribe().get((String)mapRawParent.get('type'));
            			List<Schema.ChildRelationship> lstChildRelationships = sfTypeParent.getDescribe().getChildRelationships();
						
						for(Schema.ChildRelationship childRelationship : lstChildRelationships){
							if(childRelationship.getRelationshipName()!= null && childRelationship.getRelationshipName().equalsIgnoreCase(relationshipName) == true){
								objChildType = childRelationship.getChildSObject();
								strRelatedField = childRelationship.getField().getDescribe().getName();
								break;
							}
						}
						if(objChildType == null){
							mapRawParent.put('Message','Invalid Relationship Name');
							lstMessages.add(JSON.serialize(rawParentObject));
                			Database.rollback(sp);
                			
           	 				responseMessage(400,'['+String.join(lstMessages,',')+']');
           	 				return;
						}
						for(Object rawChildObject : (List<Object>)mapRawChildren.get('records')){
							Map<String,Object> mapSfChild = (Map<String,Object>)rawChildObject;
							Map<String,Schema.SObjectField> mapfields = objChildType.getDescribe().fields.getMap();
							String validate = validateObjectField(objChildType,mapSfChild);
							if(String.isEmpty(validate) == true){
								SObject sobj = createObject(objChildType,mapSfChild);
								sobj.put(strRelatedField,mapRefSFParentObject.get((String)mapRawParent.get('referenceId')).Id);
			                    mapRefSFChildObject.put((String)mapSfChild.get('referenceId'),sobj);
			                    lstSfChildObjects.add(sobj);
							}
							else{
								mapSfChild.put('message',String.format(Label.InvalidFieldNameMessage , new String []{validate}));
			                    lstMessages.add(JSON.serialize(rawChildObject));
			                    
			   	 				responseMessage(400,'['+String.join(lstMessages,',')+']');
			   	 				return;
							}
						}
					}
        		}
			}
		}

		try{
			insert lstSfChildObjects;
		}
		catch(Exception e){
			Database.rollback(sp);
            responseMessage(400,'{"message":"Record Insertion Failed","statuscode":"400"}');
            return;
		}
		mapRefSFParentObject.putAll(mapRefSFChildObject);
		String msg = '';
		List<String> lstMessage = new List<String>();
		for(String ref : mapRefSFParentObject.keySet()){
			msg = '{';
				msg += '"Message":'+'"Records Inserted Successfully",';
				msg += '"Reference Id":'+'"'+ref+'",';
				msg += '"Id":'+'"'+mapRefSFParentObject.get(ref).Id+'"';
			msg += '}';
			lstMessage.add(msg);
		}
		
        responseMessage(200,'{"records":['+String.join(lstMessage,',')+'],"statuscode":"200"}');
	}

	/*
		Name: createObject
		Description: Method will return the SObject
		Param: 
			1. objType : Pass the type of object which need to be created
				Type : SObjectType
			2. mapObjValues : Map of fields and values which need to be insert in object
				Type : Map<String,Object>
		Return: method will return a SObject
	*/
	private static SObject createObject(SObjectType objType,Map<String,Object> mapObjValues){
		SObject sObj = objType.newSObject();
		Map<String,Schema.SObjectField> mapfields = objType.getDescribe().fields.getMap();
	    for(String fieldName : mapObjValues.keyset()){
	        if(fieldName.equalsIgnoreCase('referenceId') == false && fieldName.equalsIgnoreCase('type') == false && fieldName.equalsIgnoreCase('children') == false){
	            Schema.DisplayType fielddataType = mapfields.get(fieldName).getDescribe().getType();
	            if(fielddataType == Schema.DisplayType.Integer){
	                sObj.put(fieldName,Integer.valueof(mapObjValues.get(fieldName)));
	            }
	            else{
	                sObj.put(fieldName,mapObjValues.get(fieldName));
	            }
	        }
	    }
	    return sObj;
	}

	/*
		Name: validateObjectField
		Description: Method will validate the fields that pass by json and the existing one in object
		Param: 
			1. objType : Pass the type of object which need to be validate
				Type : SObjectType
			2. mapObjValues : Map of fields and values which need to be validate in object
				Type : Map<String,Object>
		Return: Name of Field which cause error else null.
	*/
	private static String validateObjectField(SObjectType objType,Map<String,Object> mapObjValues){
		Map<String,Schema.SObjectField> mapfields = objType.getDescribe().fields.getMap();
		for(String rawParentFieldName : mapObjValues.keyset()){
	        if(rawParentFieldName.equalsIgnoreCase('referenceId')== false && rawParentFieldName.equalsIgnoreCase('children')== false && rawParentFieldName.equalsIgnoreCase('type')== false){
	            if(mapfields.keyset().contains((rawParentFieldName).toLowerCase()) == false){
		 				return rawParentFieldName;
	            }
	        }
	    }
	    return null;
	}

	/*
		Name: responseMessage
		Param:
			1. statuscode : status code of reponse i.e. 400,200,304 etc
				Type : Integer
			2. message : responseBody which need to send on API call
				Type : String
	*/
	private static void responseMessage(Integer statuscode,String messages){
		RestContext.response.addHeader('Content-Type', 'application/json');
		RestContext.response.statuscode = statuscode;
		RestContext.response.responseBody = Blob.valueOf(messages);
	}
}