@isTest
private class Test_RestHandler{
	
	static testMethod void insertObject(){
		//creation of test data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		// pass the req and resp objects to the method
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf('[{"name":"SampleAccount1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","AccountNumber":"1234567890","ReferenceId":"Ref15"},{"type":"Account","name":"SampleAccount1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","AccountNumber":"1234567890","ReferenceId":"Ref35"},{"type":"Account","name":"SampleAccount1","phon1e":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","AccountNumber":"1234567890","ReferenceId":"ref105"},{"type":"Account","name":"SampleAccount1","phon1e":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industr1y":"Banking","AccountNumber":"1234567890"}]');
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality
		Test.startTest();
			RestHandler.insertObject();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains(Label.RecordInsertedMessage));
		System.assert(response.contains(Label.TypeNotExistsMessage));
		System.assert(response.contains('Field does not exists : '));
		System.assert(response.contains(Label.RecordInsertedMessage));
	}

	static testMethod void insertObjectJSONError() {
		//Creation of Test Data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		
		// pass the req and resp objects to the method     
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf('["type": "Contact","FirstName":"Arun","LastName": "Jain","ReferenceId":"Ref35"},{"FirstName":"Arun","LastName": "Jain","ReferenceId":"Ref35"},{"type": "Contact","FirstName":"Arun","LastName": "Jain"},{"type": "Contact","FirstName":"Arun","LastName": "Jain","ReferenceId":"Ref35"},{FirstName":"Arun","LastName": "Jain","ReferenceId":"Ref35"}]');
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality
		Test.startTest();
			RestHandler.insertObject();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains(Label.InvalidJsonFormMessage));
	}
}