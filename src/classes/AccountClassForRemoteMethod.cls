public with sharing class AccountClassForRemoteMethod {
	public AccountClassForRemoteMethod() {
		
	}

	@RemoteAction
	public static List<Account> searchAccounts(String accName){
		accName = '%'+accName+'%';
		return [SELECT Id,Name FROM Account WHERE Name LIKE :accName];
	}
}