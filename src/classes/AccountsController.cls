public class AccountsController {
	@AuraEnabled
    public static List<Account> getAccounts(){
        return [
            	SELECT Id,AccountNumber, name, industry, Type, NumberOfEmployees, TickerSymbol, Phone
			   	FROM Account
                ORDER BY CreatedDate asc
               ];
    }
}