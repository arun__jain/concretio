@isTest
private class Test_RestHandlerForChild{
	static testMethod void insertObject(){
		//creation of test data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		String welljson = '[{"type":"Account","referenceId":"ref1","name":"Sample2Account1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"relationshipName":"Contacts","records":[{"referenceId":"ref2","lastname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","lastname":"Ev2ans","title":"Vi2cePresident","email":"sample@salesforce.com"}]},{"relationshipName":"Cases","records":[{"referenceId":"ref2","Status":"New","Origin":"Web"}]}]}]';
		
		// pass the req and resp objects to the method
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf(welljson);
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality for valid json
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains('Id'));
	}

	static testMethod void testTypeNotExists(){
		//creation of test data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		String notypejson = '[{"referenceId":"ref1","name":"Sample2Account1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"relationshipName":"Contacts","records":[{"referenceId":"ref2","lastname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","lastname":"Ev2ans","title":"Vi2cePresident","email":"sample@salesforce.com"}]},{"relationshipName":"Cases","records":[{"referenceId":"ref2","Status":"New","Origin":"Web"}]}]}]';
		
		// pass the req and resp objects to the method
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf(notypejson);
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality for valid json
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains(Label.TypeNotExistsMessage));
	}

	static testMethod void testReferencenotexistsjson(){
		//creation of test data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		String referencenotexistsjson = '[{"type":"Account","name":"Sample2Account1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"relationshipName":"Contacts","records":[{"referenceId":"ref2","lastname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","lastname":"Ev2ans","title":"Vi2cePresident","email":"sample@salesforce.com"}]},{"relationshipName":"Cases","records":[{"Status":"New","Origin":"Web"}]}]}]';
	
		// pass the req and resp objects to the method
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf(referencenotexistsjson);
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality for valid json
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains('Reference Id'));
	}

	static testMethod void testInvalidFieldInParent(){
		//creation of test data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		String referencenotexistsjson = '[{"type":"Account","referenceId":"ref1","nam1e":"Sample2Account1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"relationshipName":"Contacts","records":[{"referenceId":"ref2","lastname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","lastname":"Ev2ans","title":"Vi2cePresident","email":"sample@salesforce.com"}]},{"relationshipName":"Cases","records":[{"referenceId":"ref2","Status":"New","Origin":"Web"}]}]}]';
		
		// pass the req and resp objects to the method
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf(referencenotexistsjson);
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality for valid json
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains('Reference Id'));
	}

	static testMethod void testRelationshipNameNotExists(){
		//creation of test data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		String relationshipNamenotexistsjson = '[{"type":"Account","referenceId":"ref1","name":"Sample2Account1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"records":[{"referenceId":"ref2","lastname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","lastname":"Ev2ans","title":"Vi2cePresident","email":"sample@salesforce.com"}]},{"relationshipName":"Cases","records":[{"referenceId":"ref2","Status":"New","Origin":"Web"}]}]}]';
		
		// pass the req and resp objects to the method
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf(relationshipNamenotexistsjson);
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality for valid json
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains('Relationship Name does not exists'));
	}

	static testMethod void testRecordsNotExists(){
		//creation of test data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		String relationshipNamenotexistsjson = '[{"type":"Account","referenceId":"ref1","name":"Sample2Account1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"relationshipName":"Contacts","records":[{"referenceId":"ref2","lastname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","lastname":"Ev2ans","title":"Vi2cePresident","email":"sample@salesforce.com"}]},{"relationshipName":"Cases"}]}]';
		
		// pass the req and resp objects to the method
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf(relationshipNamenotexistsjson);
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality for valid json
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains('Relationship Name does not exists'));
	}

	static testMethod void testInvalidRelationshipName(){
		//creation of test data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		String invalidRelationshipNameJson = '[{"type":"Account","referenceId":"ref1","name":"Sample2Account1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"relationshipName":"Contac1ts","records":[{"referenceId":"ref2","lastname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","lastname":"Ev2ans","title":"Vi2cePresident","email":"sample@salesforce.com"}]}]}]';
		
		// pass the req and resp objects to the method
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf(invalidRelationshipNameJson);
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality for valid json
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains('Relationship Name does not exists'));
	}

	static testMethod void testInvalidFieldNameInChild(){
		//creation of test data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		String invalidRelationshipNameJson = '[{"type":"Account","referenceId":"ref1","name":"Sample2Account1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"relationshipName":"Contacts","records":[{"referenceId":"ref2","las1tname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","lastname":"Ev2ans","title":"Vi2cePresident","email":"sample@salesforce.com"}]}]}]';
		
		// pass the req and resp objects to the method
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf(invalidRelationshipNameJson);
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality for valid json
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains('Relationship Name does not exists'));
	}

	static testMethod void insertObjectJSONError() {
		//Creation of Test Data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		
		// pass the req and resp objects to the method     
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf('[{"type":"Account","referenceId":"ref1","name":"Sample2Account1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"relationshipName":"Contacts","records":[{"referenceId":"ref2","lastname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","lastname":"Ev2ans","title":"Vi2cePresident","email":"sample@salesforce.com"}]},{"relationshipName":"Cases","records":[{"referenceId":"ref2","Status":"New","Origin":"Web"}]}]');
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains(Label.InvalidJsonFormMessage));
	}

	static testMethod void dmlExceptionTest() {
		//Creation of Test Data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		
		// pass the req and resp objects to the method     
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf('[{"type":"Account","referenceId":"ref1","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"relationshipName":"Contacts","records":[{"referenceId":"ref2","lastname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","lastname":"Ev2ans","title":"Vi2cePresident","email":"sample@salesforce.com"}]},{"relationshipName":"Cases","records":[{"referenceId":"ref2","Status":"New","Origin":"Web"}]}]}]');
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains('Record Insertion Failed'));
	}

	static testMethod void dmlExceptionChildTest() {
		//Creation of Test Data
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		
		// pass the req and resp objects to the method     
		req.requestURI = '/services/apexrest/Handle';  
		req.httpMethod = 'POST';
		req.RequestBody = Blob.valueOf('[{"type":"Account","referenceId":"ref1","name":"Sample account","phone":"1234567890","website":"www.salesforce.com","numberOfEmployees":"100","industry":"Banking","children":[{"relationshipName":"Contacts","records":[{"referenceId":"ref2","lastname":"Sm2ith","Title":"Pr2esident","email":"sample@salesforce.com"},{"referenceId":"ref3","title":"Vi2cePresident","email":"sample@salesforce.com"}]},{"relationshipName":"Cases","records":[{"referenceId":"ref2","Status":"New","Origin":"Web"}]}]}]');
		RestContext.request = req;
		RestContext.response = res;

		//testing of the functionality
		Test.startTest();
			RestHandlerForChild.insertRecords();
		Test.stopTest();

		//Check the response
		String response = res.responseBody.toString();
		System.assert(response.contains('Record Insertion Failed'));
	}
}