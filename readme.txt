1. Synopsis
	Name : Salesforce Custom Rest API
	Description : This api will create records along with their children records. It can handle One down relationship between objects. We can pass multiple children which belongs to single object.
	File Name : RestHandlerForChild.cls
	TestClass Name : Test_RestHandlerForChild.cls
2. Prerequisites
	To use API you must have JSON data in the below format:	
	A. Format for Parent and Child JSON
	[{
		"type": "Account",
		"referenceId": "ref1",
		"name": "SampleAccount1",
		"phone": "1234567890",
		"website": "www.salesforce.com",
		"numberOfEmployees": "100",
		"industry": "Banking",
		"children": [{
			"relationshipName": "Contacts",
			"records": [{
				"referenceId": "ref2",
				"lastname": "Smith",
				"Title": "President",
				"email": "sample@salesforce.com"
			}, {
				"referenceId": "ref3",
				"lastname": "Evans",
				"title": "Vice President",
				"email": "sample@salesforce.com"
			}]
		}]
	}]
	
	B. Format for Only Parent JSON
	[{
		"type": "Account",
		"referenceId": "ref1",
		"name": "SampleAccount1",
		"phone": "1234567890",
		"website": "www.salesforce.com",
		"numberOfEmployees": "100",
		"industry": "Banking"
	}]
	
	NOTE :
		1. Must Specify the "Type" In parent Object.
		2. Must specify the "referenceId" In every Record
		3. To specify multiple records use , (comma) as separator
		4. This API can handle 500 Records at a call

3. Installing
	To use API the url is:
	https://***yourInstance***.salesforce.com/apexrest/HandleWithChild
	
	Method of Request will be:
	POST
	RequestBody will be the JSON of records
	
4. Running the tests
	These are the Test Scenario for API Call
	
	1. Invalid JSON Format : Suppose by mistake the Malform JSON has passed to API call In this case the output will be:
	REQUEST:
	[{
		"type": "Account",
		"referenceId": "ref1",
		"name": "SampleAccount1",
		"phone": "1234567890",
		"website": "www.salesforce.com",
		"numberOfEmployees": "100",
		"industry": "Banking",
	}]
	
	The JSON is not Valid as we have an extra comma at the end of "industry"
	
	RESPONSE:
	{
		"statuscode":"400",
		"message":"Invalid Form of JSON in Request."
	}

	2. Type is not defined : If the type is missing in JSON request.
	REQUEST:
	[{
		"referenceId": "ref1",
		"name": "SampleAccount1",
		"phone": "1234567890",
		"website": "www.salesforce.com",
		"numberOfEmployees": "100",
		"industry": "Banking"
	}]
	
	RESPONSE:
	[{
		"Message":"Specify the Object type",
		"industry":"Banking",
		"numberOfEmployees":"100",
		"website":"www.salesforce.com",
		"phone":"1234567890",
		"name":"SampleAccount1",
		"referenceId":"ref1"
	}]
	
	3. Reference Id is not Defined : if Reference Id is missing in JSON request.
	REQUEST:
	[{
		"type": "Account",
		"name": "SampleAccount1",
		"phone": "1234567890",
		"website": "www.salesforce.com",
		"numberOfEmployees": "100",
		"industry": "Banking"
	}]
	
	RESPONSE:
	[{
		"Message":"Reference Id does not exists.",
		"industry":"Banking",
		"numberOfEmployees":"100",
		"website":"www.salesforce.com",
		"phone":"1234567890",
		"name":"SampleAccount1",
		"type":"Account"
	}]
	
	4. Relationship Not Defined : If Relationship Attribute is missing in JSON request.
	REQUEST:
	[{
		"type": "Account",
		"referenceId": "ref1",
		"name": "SampleAccount1",
		"phone": "1234567890",
		"website": "www.salesforce.com",
		"numberOfEmployees": "100",
		"industry": "Banking",
		"children": [{
			"relationshipName": "Contacts",
			"records": [{
				"referenceId": "ref2",
				"lastname": "Smith",
				"Title": "President",
				"email": "sample@salesforce.com"
			}, {
				"referenceId": "ref3",
				"lastname": "Evans",
				"title": "Vice President",
				"email": "sample@salesforce.com"
			}]
		}]
	}]
	RESPONSE:
	[{
		"children": [{
			"Message": "Relationship Name does not Exists",
			"records": [{
				"email": "sample@salesforce.com",
				"Title": "President",
				"lastname": "Smith",
				"referenceId": "ref2"
			}, {
				"email": "sample@salesforce.com",
				"title": "Vice President",
				"lastname": "Evans",
				"referenceId": "ref3"
			}]
		}],
		"industry": "Banking",
		"numberOfEmployees": "100",
		"website": "www.salesforce.com",
		"phone": "1234567890",
		"name": "SampleAccount1",
		"referenceId": "ref1",
		"type": "Account"
	}]
	
	5. Invalid Relationship Name : If relationship name is invalid according to the salesforce
	REQUEST:
	[{
		"type": "Account",
		"referenceId": "ref1",
		"name": "SampleAccount1",
		"phone": "1234567890",
		"website": "www.salesforce.com",
		"numberOfEmployees": "100",
		"industry": "Banking",
		"children": [{
			"relationshipName": "Contacts1",
			"records": [{
				"referenceId": "ref2",
				"lastname": "Smith",
				"Title": "President",
				"email": "sample@salesforce.com"
			}, {
				"referenceId": "ref3",
				"lastname": "Evans",
				"title": "Vice President",
				"email": "sample@salesforce.com"
			}]
		}]
	}]
	RESPONSE:
	[{
		"Message": "Invalid Relationship Name",
		"children": [{
			"records": [{
				"email": "sample@salesforce.com",
				"Title": "President",
				"lastname": "Smith",
				"referenceId": "ref2"
			}, {
				"email": "sample@salesforce.com",
				"title": "Vice President",
				"lastname": "Evans",
				"referenceId": "ref3"
			}],
			"relationshipName": "Contacts1"
		}],
		"industry": "Banking",
		"numberOfEmployees": "100",
		"website": "www.salesforce.com",
		"phone": "1234567890",
		"name": "SampleAccount1",
		"referenceId": "ref1",
		"type": "Account"
	}]
	6. Records Not Exists : If Child Records are missing in JSON Request.
	REQUEST:
	[{
		"type": "Account",
		"referenceId": "ref1",
		"name": "TEST",
		"phone": "1234567890",
		"website": "www.salesforce.com",
		"numberOfEmployees": "100",
		"industry": "Banking",
		"children": [{
			"relationshipName": "Contacts"
		}]
	}]
	RESPONSE:
	[{
		"children": [{
			"Message": "Records does not Exists.",
			"relationshipName": "Contacts"
		}],
		"industry": "Banking",
		"numberOfEmployees": "100",
		"website": "www.salesforce.com",
		"phone": "1234567890",
		"name": "TEST",
		"referenceId": "ref1",
		"type": "Account"
	}]
	
	7. DML Exception : If there is any DML error in Inserting the Record.
	REQUEST:
	[{
		"type": "Account",
		"referenceId": "ref1",
		"phone": "1234567890",
		"website": "www.salesforce.com",
		"numberOfEmployees": "100",
		"industry": "Banking",
		"children": [{
			"relationshipName": "Contacts",
			"records": [{
				"referenceId": "ref2",
				"lastname": "Smith",
				"Title": "President",
				"email": "sample@salesforce.com"
			}, {
				"referenceId": "ref3",
				"lastname": "Evans",
				"title": "Vice President",
				"email": "sample@salesforce.com"
			}]
		}]
	}]
	RESPONSE:
	{
		"message": "Record Insertion Failed",
		"statuscode": "400"
	}

5. Deployment
	If the request is valid with all the values and validations then
	REQUEST:
	[{
		"type": "Account",
		"referenceId": "ref1",
		"name": "SampleAccount1",
		"phone": "1234567890",
		"website": "www.salesforce.com",
		"numberOfEmployees": "100",
		"industry": "Banking",
		"children": [{
			"relationshipName": "Contacts",
			"records": [{
				"referenceId": "ref2",
				"lastname": "Smith",
				"Title": "President",
				"email": "sample@salesforce.com"
			}, {
				"referenceId": "ref3",
				"lastname": "Evans",
				"title": "Vice President",
				"email": "sample@salesforce.com"
			}]
		}]
	}]
	RESPONSE:
	{
		"records": [{
			"Message": "Records Inserted Successfully",
			"Reference Id": "ref1",
			"Id": "0017F000007PzSyQAK"
		}, {
			"Message": "Records Inserted Successfully",
			"Reference Id": "ref2",
			"Id": "0037F00000286T9QAI"
		}, {
			"Message": "Records Inserted Successfully",
			"Reference Id": "ref3",
			"Id": "0037F00000286TAQAY"
		}],
		"statuscode": "200"
	}